import sys
from aiohttp import web
from contextlib import contextmanager
import sqlite3
from datetime import date

con = sqlite3.connect(sys.argv[1])


@contextmanager
def transaction():
    cur = con.cursor()
    try:
        yield cur
    except Exception as e:
        con.rollback()
        raise
    else:
        con.commit()


with transaction() as cur:
    cur.execute(
        'CREATE TABLE IF NOT EXISTS people ('
        'id integer PRIMARY KEY AUTOINCREMENT, '
        'name varchar(25) NOT NULL'
        ')')
    cur.execute(
        'CREATE TABLE IF NOT EXISTS events ('
        'id integer PRIMARY KEY AUTOINCREMENT, '
        'description varchar(25) NOT NULL, '
        't date NOT NULL'
        ')')
    cur.execute(
        'CREATE TABLE IF NOT EXISTS attendees ('
        'id integer PRIMARY KEY AUTOINCREMENT, '
        'evt int unsigned NOT NULL REFERENCES events (id), '
        'who int unsigned NOT NULL REFERENCES people (id), '
        'attending boolean NOT NULL, '
        't timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, '
        'UNIQUE (evt, who)'
        ')')


css = open('static/main.css').read()
event_template = open('templates/quorate.html').read()
login_template = open('templates/who_are_ya.html').read()
create_event_form = open('templates/create_event.html').read()
created_event_template = open('templates/created_event.html').read()
upcoming_template = open('templates/upcoming.html').read()


def get_user(request):
    user_id = request.cookies.get('user_id')
    if user_id:
        return user_id
    raise web.HTTPSeeOther('/people/login?return_to=' + request.raw_path)


async def attendable_get(request):
    user = get_user(request)
    attendable = request.match_info.get('attendable')
    with transaction() as cur:
        cur.execute(
            'SELECT name FROM attendees JOIN people ON attendees.who = people.id '
            'WHERE evt = ? AND attending = TRUE',
            (attendable,))
        attendees = [i[0] for i in cur]
        n_attending = len(attendees)
        cur.execute('SELECT name FROM people WHERE id = ?', (user,))
        username, = cur.fetchone()
        attending = username in attendees
        cur.execute('SELECT description, t FROM events WHERE id = ?', (attendable,))
        evt_desc, evt_when = cur.fetchone()
    if attending:
        attendee_summary = 'Attending: ' + ', '.join(attendees)
    else:
        attendee_summary = ''
    # Hard coded for now:
    n_required = 6
    return web.Response(
        content_type='text/html',
        text=event_template.format(
            description=evt_desc,
            when=evt_when,
            username=username,
            n_attending=n_attending,
            attendee_summary=attendee_summary,
            in_btn_cls='chosen' if attending else 'unchosen',
            in_attrs='disabled' if attending else '',
            out_btn_cls='unchosen' if attending else 'chosen',
            out_attrs='' if attending else 'disabled',
            n_required=n_required,
            happening_cls='happening' if n_attending < n_required else 'not-happening' ))


async def attendable_post(request):
    user = get_user(request)
    attendable = request.match_info['attendable']
    data = await request.post()
    attending = 'going' in data
    with transaction() as cur:
        cur.execute(
            'INSERT INTO attendees (evt, who, attending) VALUES (?, ?, ?) '
            'ON CONFLICT (evt, who) DO UPDATE SET attending = ?, t = CURRENT_TIMESTAMP',
            (attendable, user, attending, attending))
    r = await attendable_get(request)
    return r


async def create_user(request):
    data = await request.post()
    name = data['name']
    return_path = request.query.get('return_to')
    with transaction() as c:
        c.execute('SELECT id FROM people WHERE name = ?', (name,))
        r = c.fetchone()
        if r:
            user_id = r[0]
        else:
            c.execute('INSERT INTO people (name) VALUES (?)', (name,))
            user_id = c.lastrowid
    response = web.HTTPSeeOther(return_path or '/')
    response.cookies['user_id'] = str(user_id)
    return response


async def login_form(request):
    return_to = request.query.get('return_to')
    submit_to = '/people?return_to=' + return_to if return_to else '/people'
    return web.Response(
        content_type='text/html',
        text=login_template.format(submit_to=submit_to))


async def new_event(request):
    data = await request.post()
    with transaction() as c:
        c.execute(
            'INSERT INTO events (description, t) VALUES (?, ?)',
            (data['description'], data['when']))
        event_id = c.lastrowid
    return web.HTTPCreated(
        content_type='text/html',
        text=created_event_template.format(
            attender_url='{}://{}/attending/{}'.format(request.scheme, request.host, event_id)))


async def upcoming_get(request):
    with transaction() as c:
        c.execute(
            'SELECT id, t, description FROM events WHERE t >= ? ORDER BY t',
            (date.today().isoformat(),))
        entries = [
            '<tr><td class="talignl">{}</td><td class="talignr"><a href="/attending/{}">{}</a></td></tr>'.format(t, id_, d) for id_, t, d in c]
    return web.Response(
        content_type='text/html',
        text=upcoming_template.format(entries='\n'.join(entries)))


app = web.Application()
app.add_routes([
    web.get('/attending/{attendable}', attendable_get),
    web.post('/attending/{attendable}', attendable_post),
    web.get('/events/new', lambda _: web.Response(text=create_event_form, content_type='text/html')),
    web.post('/events/new', new_event),
    web.post('/people', create_user),
    # TODO: allow name changing:
    # web.put('/people/me', rename_user),
    web.get('/people/login', login_form),
    web.get('/upcoming', upcoming_get),
    web.get('/main.css', lambda _: web.Response(text=css)),
])

if __name__ == '__main__':
    web.run_app(app, port=4123)
